# Yggdrasil Mobile

Yggdrasil Mobile App

Written in HTML5 and JavaScript

## Currently working

- Splash Screen
- About page
- MOTD
- News
- Radio
- Checking if server is alive

## How to compile?

Clone this repository and build it with Phonegap or Phonegap Build. Only Android is currently supported.

## Are there any downloads?

Yes, [there are](https://build.phonegap.com/apps/2619941/share) unsigned debug builds of this repository. Windows Phone just stands there, but isn't actually supported.

## What needs to be done?

- Porting the encryption library from C# to JavaScript
- Handling file uploads and downloads
- Enable CORS on the server side for uploads and downloads
- Listing the files
- Notifications for keeping the app and the radio alive
- Some serious UI overhaul, because it looks very sporadic right now

## Contributing

Just make some pull requests here. We will appreciate this.
